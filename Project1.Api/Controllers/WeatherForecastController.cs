﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Project1.Api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class WeatherForecastController : ControllerBase
    {
        private static readonly string[] Summaries = new[]
        {
            "Freezing", "Bracing", "Chilly", "Cool", "Mild", "Warm", "Balmy", "Hot", "Sweltering", "Scorching"
        };

        private readonly ILogger<WeatherForecastController> _logger;

        public WeatherForecastController(ILogger<WeatherForecastController> logger)
        {
            _logger = logger;
        }

        [HttpGet]
        public IEnumerable<Person> Get()
        {
            var rng = new Random();
            return Enumerable.Range(1, 5).Select(index => new Person
            {
                Id = index.ToString(),
                Dui = "1111-" + index.ToString(),
                Name = "Pedro Ramirez"
            })
            .ToArray();
        }

        [HttpPost]
        public IEnumerable<Person> Post()
        {
            return new List<Person>() { new Person()
            {
                LastName = "Cruz",
                Name = "Davis"
            },new Person()
            {
                LastName = "Josue",
                Name = "Dominguez"
            } };
        }
    }
}
