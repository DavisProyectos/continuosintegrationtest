﻿using System;

namespace Project1
{
    public class Person
    {
        public string Id { get; set; }

        public string Name { get; set; }
        public string LastName { get; set; }

        public string Dui { get; set; }

    }
}
